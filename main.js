const { Client } = require('pg')
require('dotenv').config()
const client = new Client()

async function run() {
	await client.connect()
  const ownerAccountId = process.env.OWNER_ACCOUNT_ID
	const res = await client.query('SELECT "videoPlaylist".name, "videoPlaylistId", "videoPlaylistElement".id FROM "videoPlaylistElement" INNER JOIN video ON video.id = "videoPlaylistElement"."videoId" INNER JOIN "videoPlaylist" ON "videoPlaylist".id = "videoPlaylistElement"."videoPlaylistId" WHERE "videoPlaylist"."ownerAccountId" = $1 ORDER BY "video"."originallyPublishedAt" DESC', [ownerAccountId])

	let incr = 0
	for (const row of res.rows) {
		console.log({ row })
		const res = await client.query('UPDATE "videoPlaylistElement" SET position = $1 WHERE "id" = $2', [incr, row.id])
		incr++
		console.log(res)
	}
	await client.end()
}

run()
